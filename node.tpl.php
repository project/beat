  <div class="node<?php if ($sticky) { print " sticky"; } ?><?php if (!$status) { print " node-unpublished"; } ?>">
    <?php if ($picture) {
      print $picture;
    }?>
    <?php if ($page == 0) { ?><h2 class="title"><a href="<?php print $node_url?>"><?php print $title?></a></h2><?php }; ?>
    <?php if ($submitted) { ?>
<!--       <span class="submitted"><?php //print $submitted?></span> -->
      <span class="submitted"><?php print format_date($node->created, 'custom', 'd M Y'); ?></span>

      
      
    <?php } ?>
    <div class="content"><?php print $content?></div>
    <div class="meta">
      <?php if ($terms) { ?>
        <span class="taxonomy">Archiviato in <?php print $terms?></span>
      <?php } ?>
      <?php if ($links) { ?><div class="links"><?php print $links?></div><?php }; ?>
    </div>
  </div>