<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="<?php print $language->language ?>" xml:lang="<?php print $language->language ?>" dir="<?php print $language->dir ?>">

<head>
  <title><?php print $head_title ?></title>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <meta name="AUTHOR" content="FiNeX" />
  <meta name="KEYWORDS" content="finex, informatica, design, drupal, cms, web, linux, grafica, siti, css, opensource, html, ubuntu, debian, kernel, pdc, samba" />

  <?php print $head ?>
  <?php print $styles ?>
  <?php print $scripts ?>

  <script type="text/javascript"><?php /* Needed to avoid Flash of Unstyle Content in IE */ ?> </script>

  <!--[if IE]>
  <link rel="stylesheet" type="text/css" href="<?php print base_path() . path_to_theme(); ?>/ie.css">
  <![endif]-->
</head>


<body>

<div id="top"> <!--START TOP-->
  <div class="column"> <!--START COLUMN-->
  <?php if (isset($secondary_links)) { ?>
    <div id="secondary_links">
      <?php print theme('links', $secondary_links) ?>
    </div>
  <?php } ?>
  </div> <!--END COLUMN SECONDARY-->
</div> <!--END TOP-->

<div id="header"> <!--START HEADER-->
  <div class="column">
    <?php if ($site_name) { ?>
      <h1 id="sitename"><a href="<?php print $base_path ?>" title="<?php print t('Home') ?>"><?php print $site_name ?></a></h1>
    <?php } ?>
    <?php if ($site_slogan) { ?><div id="siteSlogan"><?php print $site_slogan ?></div><?php } ?>
  </div> 
</div> <!--END HEADER-->

<div id="texture">
<div id="full-content" class="column"> <!--START COLONNA CENTRALE-->

  <div id="primary"> <!--START PRIMARY-->
  <?php if (isset($primary_links)) { ?>
    <div id="primary_links">
      <?php print theme('links', $primary_links) ?>
    </div>
  <?php } ?>
  </div> <!--END PRIMARY-->

  <div id="main"> <!--START MAIN-->
    <div id="page<?php if (! $sidebar) { print " full"; } ?>"> <!--START PAGE-->

      <div id="page_content"> <!--START PAGE CONTENT-->

        <?php print $help ?>
        <?php print $messages ?>
        <?php if ($title) { ?><h1 class="title"><?php print $title ?></h1><?php } ?>
        <?php if ($tabs) { ?><div class="tabs"><?php print $tabs ?></div><?php } ?>
        <?php print $content; ?>

      </div> <!--END PAGE CONTENT-->

    </div> <!--END PAGE-->

    <div id="sidebar"> <!--START SIDEBAR-->
      <?php if ($sidebar) { ?>
        <?php print $sidebar ?>
      <?php } ?>
    </div> <!--END SIDEBAR-->

    <div class="wrapper"></div>

  </div> <!--END MAIN-->

</div> <!--END COLONNA CENTRALE-->
</div>

<div id="footer"> <!--START FOOTER-->
  <div class="column"> <!--START COLUMN FOOTER-->


    <div id="footer">
      <?php if ($footer) { ?>
        <?php print $footer ?>
      <?php } ?>
    </div>

    <div class="wrapper"></div>

    <div id="footer_bottom">
      <?php print $footer_message ?>
      <div id="author" > <!-- PLEASE DON'T REMOVE THIS SECTION -->
        Elegant template by <a href="http://www.finex.org">FiNe<strong>X</strong><em>design</em></a> &amp; <a href="http://www.themes-drupal.org">Themes Drupal.org</a>
      </div>
    </div>

  </div> <!--END COLUMN FOOTER-->
</div> <!--END FOOTER-->

<?php print $closure ?>


</body>
</html>
